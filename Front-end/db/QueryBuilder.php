<?php
require_once "exceptions/QueryException.php";
require_once "exceptions/NotFoundException.php";

require_once "core/App.php";

require_once "db/IEntity.php";

// require_once "entity/ImagenGaleria.php";

// require_once "entity/Categoria.php";

abstract class QueryBuilder implements IEntity

{

    private $table;

    private $classEntity;

    private $connection;

    public function __construct(string $table, string $classEntity)

    {
        $this->connection = App::getConnection();

        $this->table = $table;

        $this->classEntity = $classEntity;
    }



    public function findAll()

    {

        $sql = "SELECT * FROM $this->table";

        $result = $this->executeQuery($sql);

        if (empty($result))

            throw new NotFoundException("No se ha encontrado el elemento");

        return $result;
        //fetchAll, devuelve un array que contiene todas las filas del conjunto de resultados 
        //FETCH_CLASS, devuelve instancias de la clase especificada
        /*FETCH_PROPS_LATE, cuando se usa con PDO::FETCH_CLASS, se llama 
        al constructor de la clase antes de que las proiedades sean asignadas 
        desde los valores de la columna respectiva.*/
    }



    public function save(IEntity $entity): void
    {

        try {

            $parameters = $entity->toArray();

            $sql = sprintf(
                "insert into %s (%s) values (%s)",

                $this->table,

                implode(", ", array_keys($parameters)),

                ":" . implode(", :", array_keys($parameters))

            );

            $statement = $this->connection->prepare($sql);

            $statement->execute($parameters);
        } catch ( PDOException $exception) {

            throw new QueryException("Error al insertar en la BBDD.");
        }
    }

    public function executeQuery(string $sql): array
    {
        $pdoStatement = $this->connection->prepare($sql);

        if ($pdoStatement->execute() === false)

            throw new QueryException("No se ha podido ejecutar la consulta");

        return $pdoStatement->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classEntity);
    }

    public function find(int $id): IEntity
    {

        $sql = "SELECT * FROM $this->table WHERE id=$id";

        $result = $this->executeQuery($sql);

        if (empty($result)) {
            throw new NotFoundException("No se ha encontrado el elemento con id $id");
        }

        return $result[0];
    }



    public function toArray(): array
    {
        return [];
    }
}
