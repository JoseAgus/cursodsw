
<?php 
return [

    "cursodsw/Front-end/index" => "controllers/index.php",

    "cursodsw/Front-end/about" => "controllers/about.php",
   
    "cursodsw/Front-end/blog" => "controllers/blog.php",
   
    "cursodsw/Front-end/contact" => "controllers/contact.php",
   
    "cursodsw/Front-end/galeria" => "controllers/galeria.php",
   
    "cursodsw/Front-end/post" => "controllers/single_post.php"

]

?>

