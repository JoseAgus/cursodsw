<?php
class File {



    private $file; 

    private $fileName;



    public function __construct(string $fileName, array $arrTypes) {

        $this->file = $_FILES[$fileName];

        $this->fileName = $fileName;

        if (($this->file["name"] == "")) {

            throw new FileException("Debes especificar un fichero.", 1);

        }

        if ($this->file["error"] !== UPLOAD_ERR_OK) {

            switch ($this->file["error"]) {

                case UPLOAD_ERR_INI_SIZE:

                case UPLOAD_ERR_FORM_SIZE:

                    throw new FileException("El archivo supera los 2 megas.", 2);

                case UPLOAD_ERR_PARTIAL:
                    
                    throw new FileException("Ya hay 20 archivos.", 3);
                default:

                    throw new FileException("Error desconocido.", 4);

                    break;

            }            

        }

        if (in_array($this->file["type"], $arrTypes)===false) {

            throw new FileException("El formato no es valido pruebe con jpg, png o gif.", 5);
        }            

    }

    public function saveUploadFile(string $ruta){

        if(!is_uploaded_file($this->file['tmp_name']))
        {
            throw new Exception("El archivo no se ha subido mediante un formulario.",6);
        }

        $rutaDestino = $ruta.$this->file['name'];

        if(is_file($rutaDestino))
        {
            $idUnico = time();
            $this->file['name'] =  $idUnico."_".$this->file['name'];//cambia el nombre del archivo si existe otro igual
            $rutaDestino =  $ruta.$this->file['name'];
        }

        if( move_uploaded_file($this->file['tmp_name'], $rutaDestino) === false){

            throw new Exception("El archivo no se ha subido mediante un formulario.",7);

        }
    }

    public function copyFile(string $origen, string $destino){
        $rutaOrigen = $origen.$this->file['name'];
        $rutaDestino = $destino.$this->file['name'];

        if(!is_file($rutaOrigen)){

            throw new Exception("No existe el fichero $origen.",8);

        }

        if(is_file($rutaDestino)){

            throw new Exception("El fichero $destino ya existe.",9);

        }

        
        if (!copy($rutaOrigen, $rutaDestino)) {

            throw new Exception("No se ha podido copiar el fichero.",10);

        }
        

    }

        public function getFileName(){return $this->file["name"];}
    
}
?>