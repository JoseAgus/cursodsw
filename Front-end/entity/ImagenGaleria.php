<?php
require_once 'db/IEntity.php';

class ImagenGaleria implements IEntity
{
    private $id;

    private $nombre;

    private $descripcion;

    private $numVisualizaciones;

    private $numLikes;

    private $numDownloads;

    private $categoria;


    const RUTA_IMAGENES_PORTFOLIO ="images/index/portfolio/";
    public const RUTA_IMAGENES_GALLERY ="images/index/gallery/";

 


    public function __construct($id = 0,string $nombre = "", string $descripcion = "", string $categoria = "", $numVisualizaciones = 0, $numLikes = 0, $numDownloads = 0){

        $this->id = $id;

        $this->nombre = $nombre;

        $this->descripcion = $descripcion;

        $this->categoria = $categoria;

        $this->numVisualizaciones = $numVisualizaciones;

        $this->numLikes = $numLikes;

        $this->numDownloads = $numDownloads;

        

    }

    public function getURLPortfolio() : string{

        return self::RUTA_IMAGENES_PORTFOLIO . $this->getNombre();

    }

    public function getURLGallery() : string{

        return self::RUTA_IMAGENES_GALLERY . $this->getNombre();

    }
    public function getId(){ return $this->id;}
    public function getNombre(){ return $this->nombre;}
    public function getDescripcion(){ return $this->descripcion;}
    public function getCategoria(){ return $this->categoria;}
    public function getNumVisualizaciones(){ return $this->numVisualizaciones;}
    public function getNumLikes(){ return $this->numLikes;}
    public function getNumDownloads(){ return $this->numDownloads;}
    
    
    
    public function toArray(): array{

        return [

        "id"=>$this->getId(),

        "nombre"=>$this->getNombre(),

        "descripcion"=>$this->getDescripcion(),

        "numVisualizaciones"=>$this->getNumVisualizaciones(),

        "numLikes"=>$this->getNumLikes(),

        "numDownloads"=>$this->getNumDownloads(),

        "categoria"=>$this->getCategoria()

        ];

    }



}
