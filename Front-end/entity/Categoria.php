<?php
require_once 'db/IEntity.php';

class Categoria implements IEntity{
    private $id;
    private $nombre;
    private $numImagenes;
    
    public function __construct(string $nombre = "", string $numImagenes = ""){

        $this->nombre = $nombre;

        $this->numImagenes = $numImagenes;

    }

    public function getIdC(){ return $this->id;}
    public function getNombreC(){ return $this->nombre;}
    public function getNumImagenesC(){ return $this->numImagenes;}

    public function toArray(): array{

        return [

        "id"=>$this->getIdC(),

        "nombre"=>$this->getNombreC(),

        "numImagenes"=>$this->getNumImagenesC()


        ];

    }
}