<?php

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
// Create the logger
$log = new Logger('galeria');
// Now add some handlers
$log->pushHandler(new StreamHandler('logs/info.log', Logger::INFO));


try {

    $imagenGaleriaRepository = new ImagenGaleriaRepository();
    $categoriaRepository = new CategoriaRepository();


    if ($_SERVER["REQUEST_METHOD"] === "POST") {

        $categoria = trim(htmlspecialchars($_POST["categoria"]));
        $descripcion = trim(htmlspecialchars($_POST["descripcion"]));


        $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];

        $imagen = new File("imagen", $tiposAceptados);

        $imagen->saveUploadFile("/var/www/html/cursodsw/Front-end/" . ImagenGaleria::RUTA_IMAGENES_GALLERY);
        $imagen->copyFile("/var/www/html/cursodsw/Front-end/" . ImagenGaleria::RUTA_IMAGENES_GALLERY, "/var/www/html/cursodsw/Front-end/" . ImagenGaleria::RUTA_IMAGENES_PORTFOLIO);

        $mensaje = "Datos enviados";

        $imagenGaleria = new ImagenGaleria(0, $imagen->getFileName(), $descripcion, $categoria);

        $imagenGaleriaRepository->save($imagenGaleria);

        $mensaje = "Se ha guardado la imagen en la BBDD.";

        $log->info($mensaje);
        App::get("logger")->add($mensaje);

    }

    $categorias = $categoriaRepository->findAll();
    $imagenes = $imagenGaleriaRepository->findAll();
    
} catch (FileException $fileException) {

    $errores[] = $fileException->getMessage();
} catch (QueryException $queryException) {

    $errores[] = $queryException->getMessage();
} catch (AppException $appException) {

    $errores[] = $appException->getMessage();
} catch (NotFoundException $notException) {

    $errores[] = $notException->getMessage();
} catch (UnexpectedValueException $UnexpectedValueException) {

    $errores[] = $UnexpectedValueException->getMessage();
}

require_once __DIR__ . "/../views/galeria.view.php";
